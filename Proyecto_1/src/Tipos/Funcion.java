package Tipos;

import Entorno.Simbolo;
import Gramatica.GramaticaParser;

import java.util.ArrayList;

public class Funcion {
    public String nombre;
    public ArrayList<Simbolo> lparametros;
    public Object linstrucciones;
    public GramaticaParser.LdeclPContext ldeclaracionParam;
    public GramaticaParser.DeclarationMContext declMFunc;
    public String resultado;

    public Funcion(String nombre, ArrayList<Simbolo> lparametros, Object linstrucciones, GramaticaParser.DeclarationMContext declMFunc , GramaticaParser.LdeclPContext ldeclaracionParam , String resultado) {
        this.nombre = nombre;
        this.lparametros = lparametros;
        this.linstrucciones = linstrucciones;
        this.ldeclaracionParam = ldeclaracionParam;
        this.declMFunc = declMFunc;
        this.resultado = resultado;
    }
}
