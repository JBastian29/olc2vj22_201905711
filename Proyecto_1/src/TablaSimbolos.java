
import org.antlr.runtime.tree.TreeWizard;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Stack;

import Entorno.*;
import Entorno.Simbolo.*;

public class TablaSimbolos {
    
    public TablaSimbolos(){
        
    }
    //Stack<Entorno> pilaEntTS = new Stack<Entorno>();
    public void graficarTSimbolos(Entorno ento){
        //Entorno actual = this.padre;
        //Entorno ent = new Entorno();
        //HashMap.Entry<String, Simbolo> entr = Entorno.TablaSimbolo.clone();
        String junto = "";
        try{
            File f = new File("Reporte_TablaSimbolos.html");
            FileWriter w = new FileWriter(f);
            BufferedWriter bw = new BufferedWriter(w);
            PrintWriter wr = new PrintWriter(bw);
            wr.write("<html><body>\n");
            wr.write("<TABLE BORDER= 1 cellborder = 1 cellspacing=10 cellpadding=10>\n");
            wr.write("<style type=text/css> tr{text-align: center;} table {border-collapse: collapse; background: whitesmoke; margin: auto;} body{background: #DC7633; font-family: sans-serif;}</style>\n");
            wr.write("<tr><td colspan=4><b>Tabla de Simbolos Entorno</b></td></tr>\n");
            wr.write("<tr>\n");
            wr.write("<td>" + "Tipo" + "</td>\n");
            wr.write("<td>" + "Nombre" + "</td>\n");
            wr.write("<td>" + "Valor" + "</td>\n");
            wr.write("</tr>\n");
            for(HashMap.Entry<String, Simbolo> entry : ento.TablaSimbolo.entrySet()){
                Simbolo recived = entry.getValue();
                wr.write("<tr>\n");
                wr.write("<td>" + recived.tipo.toString() + "</td>\n");
                wr.write("<td>" + recived.identificador.toString() + "</td>\n");
                if(recived.valor instanceof int[] || recived.valor instanceof double[] || recived.valor instanceof String[]){
                    if(recived.valor instanceof int[]) {
                        wr.write("<td>" + Arrays.toString((int[]) recived.valor) + "</td>\n");
                    }
                    if(recived.valor instanceof double[]) {
                        wr.write("<td>" + Arrays.toString((double[]) recived.valor) + "</td>\n");
                    }
                    if(recived.valor instanceof String[]) {
                        wr.write("<td>" + Arrays.toString((String[])recived.valor) + "</td>\n");
                    }
                }else if(recived.valor instanceof int[][] || recived.valor instanceof double[][] || recived.valor instanceof String[][]){
                    if(recived.valor instanceof int[][]) {
                        wr.write("<td>" + Arrays.deepToString((int[][]) recived.valor) + "</td>\n");
                    }
                    if(recived.valor instanceof double[][]) {
                        wr.write("<td>" + Arrays.deepToString((double[][]) recived.valor) + "</td>\n");
                    }
                    if(recived.valor instanceof String[][]) {
                        wr.write("<td>" + Arrays.deepToString((String[][]) recived.valor) + "</td>\n");
                    }
                }else{
                    wr.write("<td>" + recived.valor.toString() + "</td>\n");
                }
                wr.write("</tr>\n");
            }
            for(HashMap.Entry<String, Simbolo> entry : ento.siguiente.TablaSimbolo.entrySet()){
                Simbolo recived = entry.getValue();
                wr.write("<tr>\n");
                wr.write("<td>" + recived.tipo.toString() + "</td>\n");
                wr.write("<td>" + recived.identificador.toString() + "</td>\n");
                if(recived.valor instanceof int[] || recived.valor instanceof double[] || recived.valor instanceof String[]){
                    if(recived.valor instanceof int[]) {
                        wr.write("<td>" + Arrays.toString((int[]) recived.valor) + "</td>\n");
                    }
                    if(recived.valor instanceof double[]) {
                        wr.write("<td>" + Arrays.toString((double[]) recived.valor) + "</td>\n");
                    }
                    if(recived.valor instanceof String[]) {
                        wr.write("<td>" + Arrays.toString((String[])recived.valor) + "</td>\n");
                    }
                }else if(recived.valor instanceof int[][] || recived.valor instanceof double[][] || recived.valor instanceof String[][]){
                    if(recived.valor instanceof int[][]) {
                        wr.write("<td>" + Arrays.deepToString((int[][]) recived.valor) + "</td>\n");
                    }
                    if(recived.valor instanceof double[][]) {
                        wr.write("<td>" + Arrays.deepToString((double[][]) recived.valor) + "</td>\n");
                    }
                    if(recived.valor instanceof String[][]) {
                        wr.write("<td>" + Arrays.deepToString((String[][]) recived.valor) + "</td>\n");
                    }
                }else{
                    wr.write("<td>" + recived.valor.toString() + "</td>\n");
                }
                wr.write("</tr>\n");
            }
            wr.write("</TABLE>\n");
            wr.write("</body></html>");
            wr.close();
            bw.close();


        }catch (Exception e){
            System.out.println("Error al crear el reporte de la tabla de simbolos");
            System.out.println(e);
        }
    }

}
