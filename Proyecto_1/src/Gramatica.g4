grammar Gramatica;

options { caseInsensitive = true; }

INTR    : 'integer';
DOUBLER : 'real';
STRINGR : 'string' ;
COMPLEXR : 'complex';
CHARACTERR : 'character';
BOOLEANR : 'logical';
IMPRIMIR : 'print *' ;
SUBR    : 'subroutine' ;
COMENTR  : '!';
ENDR    : 'end' ;
INTENT  : 'intent';
IN      : 'in';
IFR     : 'if';
THENR   : 'then';
ELSEIFR : 'else if';
ELSER   : 'else';
ENDIFR  : 'end if';
DOR     : 'do';
ENDOR   : 'end do';
DOWHILER : 'do while';
DIMENSIONR : 'dimension';
SIZER : 'size';
ALLOCATABLER : 'allocatable';
ALLOCATE : 'allocate';
DEALLOCATE : 'deallocate';
FUNCTIONR : 'function';


INT     : [0-9]+ ;
DOUBLE  : ([0-9]*[.])?[0-9]+ ;
CHARACTER :  ('\''|'"')(~['\r\n])('\''|'"');
BOOLEAN : ([.][t][r][u][e][.]|[.][f][a][l][s][e][.]);
COMPLEX : [9][.][1][9][2][5][1][7][9][2][6][E][-][4][3][,][0][.][0][0][0][0][0][0][0][0];
IDEN    : [a-zA-Z0-9_]+ ;
STRING  : ('\''|'"') (~['\r\n] | '""')* ('\''|'"');
COMENT  : '!'.+?('\n'|EOF)->skip;
WS      : [ \t\r]+ -> skip ;


start : lmetodos;

lmetodos : metodos (metodos)* ;

metodos : function
    | subroutine
    | program
    ;

program : 'program' n1=IDEN '\n' 'implicit' 'none' '\n' linstrucciones 'end program' n2=IDEN
;

subroutine : SUBR id1=IDEN '(' (lexpr)? ')' '\n' 'implicit' 'none' '\n' (ldeclP)? linstrucciones ENDR SUBR id2=IDEN '\n'
;

function : FUNCTIONR id1=IDEN '(' (lexpr)? ')' 'result' '(' id2=IDEN ')' '\n' 'implicit' 'none' '\n' (ldeclP)? declarationM linstrucciones ENDR FUNCTIONR id3=IDEN '\n'
;

linstrucciones : instrucciones (instrucciones) *
        ;

instrucciones : block
        | declaration
        | declarationD
        | declarationM
        | declArray
        | declArrayB
        | declDinamicA
        | asigArray
        | asigArrayB
        | asignDinamicA
        | reasignation
        | ifStructure
        | doStructure
        | doWhileStructure
        | exitC
        | cycleC
        | print
        | call
        ;



ldeclP : declParameters+ ;

declParameters : type ',' INTENT '(' IN ')' ':' ':' IDEN '\n'
        | type ',' INTENT '(' IN ')' ':' ':' expr '\n'
        ;


call : 'call' IDEN '(' lexpr ')' '\n' ;

lexpr : expr ( ',' expr )*
;

print : IMPRIMIR (',' expr)+ '\n';

block : '{' '\n' linstrucciones '}' '\n';

iden : IDEN '=' expr
    | IDEN ;

declarationM : type ':'':' lIDEN '\n';

declaration : type ':'':' IDEN '=' expr '\n';

declarationD : type ':'':' IDEN '\n';

lIDEN : iden(',' iden)* ;

reasignation : IDEN '=' expr '\n';

ifStructure : IFR '(' expr ')' THENR '\n' list1=linstrucciones (lELSEIF)* (ELSER '\n' list2=linstrucciones)? ENDIFR '\n';

lELSEIF: elseif (elseif)*;

elseif : ELSEIFR '(' expr ')' THENR '\n' linstrucciones;

doStructure : DOR IDEN '=' vinicio=expr ',' vfinal=expr (',' paso=expr)? '\n' linstrucciones ENDOR '\n';

doWhileStructure: DOWHILER '(' expr ')' '\n' linstrucciones ENDOR '\n';

exitC : 'exit' '\n';

cycleC : 'cycle' '\n';

declArray : type ',' DIMENSIONR '(' dimen=expr ')' ':'':' IDEN '\n'
        | type ':'':' IDEN '(' dimen=expr ')' '\n'
;

declArrayB : type ',' DIMENSIONR '(' dimen1=expr ',' dimen2=expr ')' ':'':' IDEN '\n'
        | type ':'':' IDEN '(' dimen1=expr ',' dimen2=expr ')' '\n'
;

asigArray : IDEN '=' '(/' lexpr '/)' '\n'
        | IDEN '[' posi=expr ']' '=' valor=expr '\n'
        | IDEN '['posi=expr (',' posi2=expr)? ']' '=' valor=expr '\n'
        ;

asigArrayB : IDEN '[' posi1=expr ']''[' posi2=expr ']' '=' valor=expr '\n' ;


declDinamicA : type ',' ALLOCATABLER ':'':' IDEN '(' dosp=':' ')' '\n'
        | type ',' ALLOCATABLER ':'':' IDEN '(' dosp=':,:' ')' '\n'
        ;

asignDinamicA : pasig = ALLOCATE '(' IDEN '(' dimen1=expr ')'  ( ',' '(' dimen2=expr ')' )? ')' '\n'
        | pasig = DEALLOCATE '(' IDEN ')' '\n'
        ;

type : INTR
    | STRINGR
    | DOUBLER
    | COMPLEXR
    | CHARACTERR
    | BOOLEANR
    ;

expr : left=expr op='**' right=expr #opExpr
   | left=expr op=('*'|'/') right=expr #opExpr
   | left=expr op=('+'|'-') right=expr #opExpr
   | '.not.' left = expr               #notExpr
   | left=expr opr=('=='|'/='|'>'|'<'|'>='|'<=' | '.eq.'|'.ne.'|'.gt.'|'.lt.'|'.ge.'|'.le.') right=expr #opExprRacional
   | left=expr opl=('.and.'|'.or.') right=expr #opExprLogica
   | '(' expr ')'                      #parenExpr
   | 'size' '(' IDEN ')'               #lengthArray
   | IDEN '['dime1=expr (',' dime2=expr)? ']'#vArrayExpr
   | atom=INT                          #atomExpr
   | vreal=DOUBLE                      #vrealExpr
   | vcharacter=CHARACTER              #vcharacterExpr
   | vboolean=BOOLEAN                  #vbooleanExpr
   | vcomplex=COMPLEX                  #vcomplexExpr
   | str=STRING                        #strExpr
   | id=IDEN                           #idExpr
   | IDEN '(' (lexpr)? ')'                #funcExpr
   ;