import Entorno.*;
import Gramatica.*;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.*;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.antlr.v4.runtime.CharStreams.fromString;

public class MainO {
    GramaticaParser sintactico;
    GramaticaParser.StartContext startCtx;

    public Entorno entRepo;
    String imprimir;
    public MainO(String input) throws IOException {

        ArrayList<ErrorCompilador> errores = new ArrayList<ErrorCompilador>();
        errores.add(new ErrorCompilador(-1,-1, "DESCRIPCIÓN", null));

        CharStream cs = fromString(input);

        GramaticaLexer lexico = new GramaticaLexer(cs);
        lexico.removeErrorListeners();
        lexico.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
            {
                errores.add(new ErrorCompilador(line, charPositionInLine, msg, ErrorCompilador.ErrorTipo.Lexico));
            }
        });
        CommonTokenStream tokens = new CommonTokenStream(lexico);
        sintactico = new GramaticaParser(tokens);
        sintactico.removeErrorListeners();
        sintactico.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
            {
                errores.add(new ErrorCompilador(line, charPositionInLine, msg, ErrorCompilador.ErrorTipo.Sintactico));
            }
        });
        startCtx = sintactico.start();


        // PRIMER PASADA > EJECUCIÓN DE INTÉRPRETE JUNTO CON VALIDACIONES SEMÁNTICAS
        entRepo = new Entorno(null);
        Visitor visitor = new Visitor(entRepo);
        visitor.visit(startCtx);
        errores.addAll(visitor.errores);

        String reportError = "digraph E { node[shape=none] tabla [label=<<TABLE>";
        for (ErrorCompilador err : errores)
            reportError += err.toString();
        reportError += "</TABLE>>]; }";

        FileWriter file = new FileWriter("errores.dot");
        file.write(reportError);
        file.close();

        Runtime.getRuntime().exec("dot -Tpdf errores.dot -o errores.pdf");

        try {
            // SEGUNDA PASADA > GENERACIÓN DE CÓDIGO EN TRES DIRECCIONES
            VisitorC3D v3D = new VisitorC3D(visitor.padre, visitor.pilaEnt);
            v3D.visit(startCtx);

            imprimir = "";
            imprimir += v3D.c3d.getHeader();
            for(String ln : v3D.c3d.codigo3D)
                imprimir += ln + '\n';
        }catch (Exception e){
            System.out.println("-----Error al general c3d-----");
            System.out.println(e);
            System.out.println("-----------------------------------------");
        }






        /*FileWriter file3d = new FileWriter("salida.c");
        file3d.write(para3D.c3d.getHeader() + "\n");
        //System.out.println(para3D.c3d.getHeader());
        for (String ln : para3D.c3d.codigo3d)
            file3d.write(ln + "\n");//System.out.println(ln);

        file3d.close();*/

    }
    public void generarCST(){
        List<String> rulesnames = Arrays.asList(sintactico.getRuleNames());
        TreeViewer treeViewer = new TreeViewer(rulesnames, startCtx);
        treeViewer.open();
    }
}