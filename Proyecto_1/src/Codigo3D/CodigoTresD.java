package Codigo3D;
import Entorno.*;

import java.util.ArrayList;

public class CodigoTresD {

    public ArrayList<String> codigo3D;
    public int temporal;
    public int label;



    public ArrayList<String> variables = new ArrayList<>();
    public CodigoTresD(){
        this.codigo3D = new ArrayList<>();
        this.temporal= -1;
        this.label = -1;
    }

    public String generateTemporal(){
        this.temporal++;
        return String.valueOf("t"+this.temporal);
    }

    public String generateLabel(){
        this.label++;
        return String.valueOf("L"+this.label);
    }
    public String lastTemporal(){
        return String.valueOf("t"+this.temporal);
    }
    private String getPrintVars()
    {
        String tempStart = this.generateTemporal();
        String labelStart = this.generateLabel();
        return "void imprimir_variable()\n" +
                "{\n" +
                tempStart + " = STACK[(int)P];\n" +
                labelStart + ":\n" +
                this.generateTemporal() + " = HEAP[(int)" + tempStart + "];\n" +
                "if (" + this.lastTemporal() + " != -1) goto L" + (this.label + 1) + ";\n" +
                "goto L" + (this.label + 2) + ";\n" +
                this.generateLabel() + ":\n" +
                "printf(\"%c\", (char)" + this.lastTemporal() + ");\n" +
                tempStart + "=" + tempStart + " + 1;\n" +
                "goto " + labelStart + ";\n" +
                this.generateLabel() + ":\n" +
                "printf(\"%c\\n\", (char)32);\n" +
                "return;\n" +
                "}\n\n";
    }
    private String getPrintStr()
    {
        String tempStart = this.generateTemporal();
        String labelStart = this.generateLabel();


        return "void imprimir_string()\n" +
                "{\n" +
                tempStart + "= P    ;\n"+
                labelStart + ":\n" +
                this.generateTemporal() + " = HEAP[(int)" + tempStart + "];\n" +
                "if (" + this.lastTemporal() + " != -1) goto L" + (this.label + 1) + ";\n" +
                "goto L" + (this.label + 2) + ";\n" +
                this.generateLabel() + ":\n" +
                "printf(\"%c\", (char)" + this.lastTemporal() + ");\n" +
                tempStart + "=" + tempStart + " + 1;\n" +
                "goto " + labelStart + ";\n" +
                this.generateLabel() + ":\n" +
                "printf(\"%c\\n\", (char)32);\n" +
                "return;\n" +
                "}\n\n";
    }
    private String getPrintVarInt(){
        return "void imprimir_var_int(){\n" +
                this.generateTemporal() + " = STACK[(int)P] ; \n" +
                "printf(\"%f\\n\", " + this.lastTemporal() + ");\n"+
                "return;\n}\n\n";
    }

    public String getHeader(){
        String prints = this.getPrintVars() + this.getPrintVarInt() + this.getPrintStr();
        //Para obtener listado de termporales t1, t2, t3
        String temporales = "";
        for (int i = 0; i <=this.temporal; i++){
            temporales += "t"+ String.valueOf(i)+(i < this.temporal ? "," : ";\n");
        }
        String variables = "";
        for (int i = 0; i < this.variables.size(); i++){
            variables +=String.valueOf(this.variables.get(i)) + (i<this.variables.size()-1 ? ",": ";\n");
        }
        if(variables != ""){
            return "#include <stdio.h>\n" +
                    "double STACK[30101999];\n" +
                    "double HEAP[30101999];\n" +
                    "double P;\n" +
                    "double H;\n" +
                    "double " + temporales+"\n" +
                    "double "+ variables+"\n"+
                    "\n" + prints;
        }else{
            return "#include <stdio.h>\n" +
                    "double STACK[30101999];\n" +
                    "double HEAP[30101999];\n" +
                    "double P;\n" +
                    "double H;\n" +
                    "double " + temporales+"\n" +
                    "\n" + prints;
        }



    }

}
