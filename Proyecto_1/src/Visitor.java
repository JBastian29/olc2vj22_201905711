

import Entorno.*;
import Entorno.Simbolo.*;
import Gramatica.*;
import Tipos.Program;
import Tipos.Subrutina;
import Tipos.Funcion;

import javax.swing.plaf.synth.SynthMenuItemUI;
import java.nio.file.SimpleFileVisitor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class Visitor extends GramaticaBaseVisitor<Object> {

    Stack<Entorno> pilaEnt = new Stack<Entorno>();
    ArrayList<ErrorCompilador> errores = new ArrayList<ErrorCompilador>();

    Entorno padre;


    boolean esfuncion;
    public Visitor(Entorno ent) {
        this.pilaEnt.push(ent);
        this.padre = ent;
    }

    public Object visitStart(GramaticaParser.StartContext ctx)
    {
        visitLmetodos(ctx.lmetodos());
        return true;
    }
    public Object visitProgram(GramaticaParser.ProgramContext ctx){
        if(ctx.n1 == null || ctx.n2 == null){
            System.out.println(" **Unexpected end of file** Debe existir PROGRAM/ENDPROGRAM");
            return true;
        }
        String n1=ctx.n1.getText();
        String n2=ctx.n2.getText();
        if(n1.equals(n2)){
            Entorno blck = new Entorno(pilaEnt.peek());
            if(!pilaEnt.peek().TablaSimbolo.containsKey(ctx.n1.getText().toUpperCase()+TipoSimbolo.Program.name().toUpperCase())) {
                Program subr = new Program(ctx.n1.getText(),ctx.linstrucciones());
                pilaEnt.peek().nuevoSimbolo(ctx.n1.getText() + TipoSimbolo.Program.name(),
                        new Simbolo(ctx.n1.getText(), "Program", subr, TipoSimbolo.Program));
                pilaEnt.peek().siguiente = blck;
                pilaEnt.push(blck);
                visitLinstrucciones(ctx.linstrucciones());
            }
        }else{
            System.out.println(" **Unexpected end of file** Debe coincidir PROGRAM/ENDPROGRAM");
            return true;
        }
        return true;
    }

    public Object visitLinstrucciones(GramaticaParser.LinstruccionesContext ctx)
    {
        for (GramaticaParser.InstruccionesContext ictx : ctx.instrucciones()) {
            String tx=ictx.getText();
            Object var = visitInstrucciones(ictx);
            if(var instanceof Integer){
                return var;
            }
            if (!((boolean) var))
                return false;
        }
        return true;
    }

    public Object visitLmetodos(GramaticaParser.LmetodosContext ctx){
        for (GramaticaParser.MetodosContext ictx : ctx.metodos()){
            visitMetodos(ictx);
        }
        return true;
    }

    public Object visitMetodos(GramaticaParser.MetodosContext ctx){
        if (ctx.function() != null){
            visitFunction(ctx.function());
        }else if (ctx.subroutine() != null){
            visitSubroutine(ctx.subroutine());
        }else if (ctx.program() != null){
            visitProgram(ctx.program());
        }
        return true;
    }

    public Object visitInstrucciones(GramaticaParser.InstruccionesContext ctx)
    {
        //System.out.println("---antes if--");
        //System.out.println(ctx.getText());
        if (ctx.block() != null)
            visitBlock(ctx.block());
        else if (ctx.declaration() != null)
            visitDeclaration(ctx.declaration());
        else if(ctx.declarationD() != null)
            visitDeclarationD(ctx.declarationD());
        else if(ctx.declarationM()!= null)
            visitDeclarationM(ctx.declarationM());
        else if(ctx.declArray()!= null)
            visitDeclArray(ctx.declArray());
        else if(ctx.declArrayB()!= null)
            visitDeclArrayB(ctx.declArrayB());
        else if(ctx.declDinamicA()!= null)
            visitDeclDinamicA(ctx.declDinamicA());
        else if(ctx.asigArray()!= null)
            visitAsigArray(ctx.asigArray());
        else if(ctx.asigArrayB()!= null)
            visitAsigArrayB(ctx.asigArrayB());
        else if(ctx.asignDinamicA()!= null)
            visitAsignDinamicA(ctx.asignDinamicA());
        /*else if(ctx.function()!= null)
            visitFunction(ctx.function());*/
        else if(ctx.reasignation()!= null)
            visitReasignation(ctx.reasignation());
        else if (ctx.ifStructure() != null)
            return visitIfStructure(ctx.ifStructure());
        else if (ctx.doStructure() != null)
            return visitDoStructure(ctx.doStructure());
        else if (ctx.doWhileStructure() != null)
            visitDoWhileStructure(ctx.doWhileStructure());
        else if (ctx.cycleC() != null)
            return visitCycleC(ctx.cycleC());
        else if (ctx.exitC() != null)
            return visitExitC(ctx.exitC());
        else if (ctx.print() != null)
            visitPrint(ctx.print());
        /*else if (ctx.subroutine() != null)
            visitSubroutine(ctx.subroutine());*/
        else if (ctx.call() != null)
            visitCall(ctx.call());
        return true;
    }

    public Object visitSubroutine(GramaticaParser.SubroutineContext ctx)
    {
        if (ctx.id1.getText().equals(ctx.id2.getText()))
        {
            if(!pilaEnt.peek().TablaSimbolo.containsKey((ctx.id1.getText() + TipoSimbolo.Subrutina.name()).toUpperCase()))
            {
                Subrutina subr;
                ArrayList<Simbolo> parametros = new ArrayList<Simbolo>();
                if(ctx.ldeclP() != null && ctx.lexpr() != null) {
                    for (GramaticaParser.ExprContext expCtx : ctx.lexpr().expr())
                        parametros.add(new Simbolo(expCtx.getText(), "", null, TipoSimbolo.Parametros));
                    subr = new Subrutina(ctx.id1.getText(), parametros, ctx.linstrucciones(), ctx.ldeclP());
                }else {
                    subr = new Subrutina(ctx.id1.getText(), parametros, ctx.linstrucciones(), null);
                }
                pilaEnt.peek().nuevoSimbolo(ctx.id1.getText() + TipoSimbolo.Subrutina.name(),
                        new Simbolo(ctx.id1.getText(), "Subrutina", subr, TipoSimbolo.Subrutina));
                return true;
            }
        }
        errores.add(new ErrorCompilador(ctx.id1.getLine(), ctx.id1.getCharPositionInLine(),
                "Los identificadores de la subrutina no coinciden", ErrorCompilador.ErrorTipo.Semantico));
        return true;
    }

    public Object visitFuncExpr(GramaticaParser.FuncExprContext ctx){
        System.out.println("Entre a la funcion" + ctx.IDEN().getText());
        Entorno ent = pilaEnt.peek();
        Simbolo simbRutina = ent.Buscar(ctx.IDEN().getText()+TipoSimbolo.Funcion.name());
        Entorno entSubr = new Entorno(ent);
        Funcion subr = (Funcion) simbRutina.valor;
        if(ctx.lexpr() != null) {
            if (subr.lparametros.size() == ctx.lexpr().expr().size() && subr.lparametros.size() == subr.ldeclaracionParam.getChildCount()) {
                for (int i = 0; i < ctx.lexpr().expr().size(); i++) {
                    subr.lparametros.get(i).valor = ((Simbolo) visit(ctx.lexpr().expr().get(i))).valor;
                    subr.lparametros.get(i).tipo = subr.ldeclaracionParam.declParameters(i).type().getText().toUpperCase();
                    entSubr.nuevoSimbolo(subr.lparametros.get(i).identificador + TipoSimbolo.Variable.name(), subr.lparametros.get(i));
                }

                pilaEnt.push(entSubr);
                visitDeclarationM(subr.declMFunc);
                visitLinstrucciones((GramaticaParser.LinstruccionesContext) subr.linstrucciones);
                Simbolo retorno = pilaEnt.peek().Buscar(subr.resultado + TipoSimbolo.Variable.name());
                pilaEnt.pop();
                return retorno;
            }
        }else{
            pilaEnt.push(entSubr);
            visitDeclarationM(subr.declMFunc);
            visitLinstrucciones((GramaticaParser.LinstruccionesContext) subr.linstrucciones);
            Simbolo retorno = pilaEnt.peek().Buscar(subr.resultado + TipoSimbolo.Variable.name());
            pilaEnt.pop();
            return retorno;
        }
        return true;
    }

    public Object visitFunction(GramaticaParser.FunctionContext ctx)
    {
        if (ctx.id1.getText().equals(ctx.id3.getText()))
        {
            if(!pilaEnt.peek().TablaSimbolo.containsKey((ctx.id1.getText() + TipoSimbolo.Funcion.name()).toUpperCase()))
            {
                Funcion func;
                ArrayList<Simbolo> parametros = new ArrayList<Simbolo>();
                if(ctx.ldeclP() != null && ctx.lexpr() != null) {
                    for (GramaticaParser.ExprContext expCtx : ctx.lexpr().expr())
                        parametros.add(new Simbolo(expCtx.getText(), "", null, TipoSimbolo.Parametros));

                    func = new Funcion(ctx.id1.getText(), parametros, ctx.linstrucciones(), ctx.declarationM(),ctx.ldeclP(),ctx.id2.getText());
                }else{
                    func = new Funcion(ctx.id1.getText(), parametros, ctx.linstrucciones(), ctx.declarationM(),null,ctx.id2.getText());
                }

                pilaEnt.peek().nuevoSimbolo(ctx.id1.getText() + TipoSimbolo.Funcion.name(),
                        new Simbolo(ctx.id1.getText(), "Funcion", func, TipoSimbolo.Funcion));
                return true;
            }
        }
        errores.add(new ErrorCompilador(ctx.id1.getLine(), ctx.id1.getCharPositionInLine(),
                "Los identificadores de la funcion no coinciden", ErrorCompilador.ErrorTipo.Semantico));
        return true;
    }


    public Object visitCall(GramaticaParser.CallContext ctx)
    {
        Entorno ent = pilaEnt.peek();
        Simbolo simbRutina = ent.Buscar(ctx.IDEN().getText() + TipoSimbolo.Subrutina.name());
        if (simbRutina == null) errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(),
                ctx.IDEN().getSymbol().getCharPositionInLine(), "La subrutina " + ctx.IDEN().getText() + " no existe.",
                ErrorCompilador.ErrorTipo.Semantico));
        else {
            Entorno entSubr = new Entorno(ent);
            Subrutina subr = (Subrutina) simbRutina.valor;
            if (ctx.lexpr() != null) {
                if (subr.lparametros.size() == ctx.lexpr().expr().size() && subr.lparametros.size() == subr.ldeclaracionParam.getChildCount()) {
                    for (int i = 0; i < ctx.lexpr().expr().size(); i++) {
                        Simbolo v = (Simbolo) visit(ctx.lexpr().expr().get(i));
                        subr.lparametros.get(i).valor = v.valor;
                        subr.lparametros.get(i).tipo = subr.ldeclaracionParam.declParameters(i).type().getText().toUpperCase();
                        entSubr.nuevoSimbolo(subr.lparametros.get(i).identificador + TipoSimbolo.Variable.name(),
                                subr.lparametros.get(i));
                    }

                    pilaEnt.push(entSubr);
                    visitLinstrucciones((GramaticaParser.LinstruccionesContext) subr.linstrucciones);
                    pilaEnt.pop();
                } else
                    errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                            "La cantidad de parámetros no coincide.", ErrorCompilador.ErrorTipo.Semantico));
            }else{
                pilaEnt.push(entSubr);
                visitLinstrucciones((GramaticaParser.LinstruccionesContext) subr.linstrucciones);
                pilaEnt.pop();
            }
        }
        return true;
    }

    public Object visitCycleC(GramaticaParser.CycleCContext ctx)
    {
        return 1;
    }

    public Object visitExitC(GramaticaParser.ExitCContext ctx)
    {
        return false;
    }

    public Object visitLexpr(GramaticaParser.LexprContext ctx)
    {
        //System.out.println(ctx.getText());
        for (GramaticaParser.ExprContext ectx : ctx.expr()) {
            //System.out.println("-----expr---");
            //System.out.println(ectx.getText());
            visit(ectx);
        }
        return true;
    }

    public Object visitPrint(GramaticaParser.PrintContext ctx)
    {
        String todo = "";
        for(GramaticaParser.ExprContext expr : ctx.expr()){
            Simbolo s = (Simbolo)visit(expr);
            try {
                if(s.valor instanceof int[] || s.valor instanceof double[] || s.valor instanceof String[]){
                    if(s.valor instanceof int[]) {
                        todo=todo+Arrays.toString((int[]) s.valor);
                    }
                    if(s.valor instanceof double[]) {
                        todo=todo+ Arrays.toString((double[]) s.valor);
                    }
                    if(s.valor instanceof String[]) {
                        todo=todo+Arrays.toString((String[])s.valor);
                    }
                }else if(s.valor instanceof int[][] || s.valor instanceof double[][] || s.valor instanceof String[][]) {
                    if (s.valor instanceof int[][]) {
                        todo = todo + Arrays.deepToString((int[][]) s.valor);
                    }
                    if (s.valor instanceof double[][]) {
                        todo = todo + Arrays.deepToString((double[][]) s.valor);
                    }
                    if (s.valor instanceof String[][]) {
                        todo = todo + Arrays.deepToString((String[][]) s.valor);
                    }
                }else{
                    todo=todo+s.valor.toString();
                }

            }catch (Exception e) {
                System.out.println(e.toString());
            }
        }
        System.out.println(todo);
        return true;
    }

    public Object visitBlock(GramaticaParser.BlockContext ctx)
    {
        pilaEnt.push(new Entorno(pilaEnt.peek()));
        visitLinstrucciones(ctx.linstrucciones());
        pilaEnt.pop();
        return true;
    }

    public Object visitReasignation(GramaticaParser.ReasignationContext ctx)
    {
        Entorno ent = pilaEnt.peek();
        //System.out.println("ENTRE A LA REASIGNACION");
        //System.out.println(ctx.IDEN().getText());
        Simbolo existeToUpdate = ent.Buscar(ctx.IDEN().getText() + TipoSimbolo.Variable.name());
        if(existeToUpdate!=null){
            Simbolo toUpdate = ent.Buscar(ctx.IDEN().getText() + TipoSimbolo.Variable.name());
            Simbolo nuevo = (Simbolo)visit(ctx.expr());
            nuevo.identificador = ctx.IDEN().getText();
            if(!toUpdate.tipo.equalsIgnoreCase(nuevo.tipo.toUpperCase())){
                errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(),ctx.IDEN().getSymbol().getCharPositionInLine(),
                        "El tipo de variable no coincide para ser modificado",ErrorCompilador.ErrorTipo.Semantico));
                System.out.println("El tipo de variable no coincide para ser modificado");
                return false;
            }else{
                ent.editarSimbolo((ctx.IDEN().getText() + TipoSimbolo.Variable.name().toUpperCase()),nuevo);
                return true;
            }
        }else{
            errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(),ctx.IDEN().getSymbol().getCharPositionInLine(),
                    "La variable "+ctx.IDEN().getText() + " no existe para ser modificada",ErrorCompilador.ErrorTipo.Semantico));
            System.out.println("La variable "+ctx.IDEN().getText() +  " no existe para ser modificada");
        }
        return true;
    }

    public Object visitDeclaration(GramaticaParser.DeclarationContext ctx)
    {
        //System.out.println("ENTRE A LA DECLARACION CON VALOR");
        Entorno ent = pilaEnt.peek();
        if(!ent.TablaSimbolo.containsKey((ctx.IDEN().getText() + TipoSimbolo.Variable.name()).toUpperCase()))
        {
            Simbolo nuevo = (Simbolo)visit(ctx.expr());
            nuevo.identificador = ctx.IDEN().getText();
            nuevo.tipo = ctx.type().getText();
            if(nuevo.tipo.equalsIgnoreCase("logical")){
                if(nuevo.valor.equals(".true.") || nuevo.valor.equals("T")){
                    nuevo.valor="T";
                }else{
                    nuevo.valor="F";
                }
            }
            ent.nuevoSimbolo(ctx.IDEN().getText() + TipoSimbolo.Variable.name(), nuevo);
            return true;
        }
        else errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                "La variable ya existe en el entorno actual.", ErrorCompilador.ErrorTipo.Semantico));
        return false;
    }

    public Object visitDeclarationD(GramaticaParser.DeclarationDContext ctx)
    {
        System.out.println("ENTRE A LA DECLARACION DEFAULT");
        //System.out.println(ctx.IDEN().getText());
        Entorno ent = pilaEnt.peek();
        if(!ent.TablaSimbolo.containsKey((ctx.IDEN().getText() + TipoSimbolo.Variable.name()).toUpperCase()))
        {
            Simbolo nuevo;
            if (ctx.type().getText().equals("real")){
                nuevo = new Simbolo(null,null,0.00000000,TipoSimbolo.Variable);
            }
            else if (ctx.type().getText().equals("integer")){
                nuevo = new Simbolo(null,null,0,TipoSimbolo.Variable);
            }
            else if (ctx.type().getText().equals("complex")){
                nuevo = new Simbolo(null,null,"9.192517926E-43,0.00000000",TipoSimbolo.Variable);
            }
            else if (ctx.type().getText().equals("character")){
                nuevo = new Simbolo(null,null,"",TipoSimbolo.Variable);
            }
            else if (ctx.type().getText().equals("logical")){
                nuevo = new Simbolo(null,null,"F",TipoSimbolo.Variable);
            }
            else{
                nuevo = new Simbolo(null,null,0,TipoSimbolo.Variable);
            }
            nuevo.identificador = ctx.IDEN().getText();
            nuevo.tipo = ctx.type().getText();
            ent.nuevoSimbolo(ctx.IDEN().getText() + TipoSimbolo.Variable.name(), nuevo);
            return true;
        }
        else errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                "La variable ya existe en el entorno actual.", ErrorCompilador.ErrorTipo.Semantico));
        return false;
    }

    public Object visitDeclarationM(GramaticaParser.DeclarationMContext ctx)
    {
        //System.out.println("ENTRE DECLARACION MULTIPLE");
        Entorno ent = pilaEnt.peek();

        if(ctx.lIDEN() != null){
            for(int i=0; i<ctx.lIDEN().iden().size(); i++){
                if (!ent.TablaSimbolo.containsKey(ctx.lIDEN().iden().get(i).IDEN() + TipoSimbolo.Variable.name().toUpperCase())){
                    Simbolo nuevo;
                    if((ctx.lIDEN().iden().get(i).expr()) == null) {
                        if (ctx.type().getText().equals("real")) {
                            nuevo = new Simbolo(null, null, 0.00000000, TipoSimbolo.Variable);
                        } else if (ctx.type().getText().equals("integer")) {
                            nuevo = new Simbolo(null, null, 0, TipoSimbolo.Variable);
                        } else if (ctx.type().getText().equals("complex")) {
                            nuevo = new Simbolo(null, null, "9.192517926E-43,0.00000000", TipoSimbolo.Variable);
                        } else if (ctx.type().getText().equals("character")) {
                            nuevo = new Simbolo(null, null, "", TipoSimbolo.Variable);
                        } else if (ctx.type().getText().equals("logical")) {
                            nuevo = new Simbolo(null, null, "F", TipoSimbolo.Variable);
                        } else {
                            nuevo = new Simbolo(null, null, 0, TipoSimbolo.Variable);
                        }
                        nuevo.identificador = ctx.lIDEN().iden().get(i).IDEN().getText();
                        nuevo.tipo = ctx.type().getText();
                        ent.nuevoSimbolo(ctx.lIDEN().iden().get(i).IDEN().getText() + TipoSimbolo.Variable.name(), nuevo);
                    }else{
                        nuevo = (Simbolo)visit(ctx.lIDEN().iden().get(i).expr());
                        nuevo.identificador = ctx.lIDEN().iden().get(i).IDEN().getText();
                        nuevo.tipo = ctx.type().getText();
                        ent.nuevoSimbolo(ctx.lIDEN().iden().get(i).IDEN().getText() + TipoSimbolo.Variable.name(), nuevo);
                    }
                }
                else errores.add(new ErrorCompilador(ctx.lIDEN().iden().get(i).IDEN().getSymbol().getLine(), ctx.lIDEN().iden().get(i).IDEN().getSymbol().getCharPositionInLine(),
                        "La variable ya existe en el entorno actual.", ErrorCompilador.ErrorTipo.Semantico));
            }
        }
        return true;
    }

    public Object visitDeclArray(GramaticaParser.DeclArrayContext ctx){
        Entorno ent = pilaEnt.peek();
        if(!ent.TablaSimbolo.containsKey((ctx.IDEN().getText() + TipoSimbolo.Variable.name()).toUpperCase()))
        {
            Simbolo nuevo = new Simbolo("","",0,TipoSimbolo.Variable);
            Simbolo dimen = (Simbolo)visit(ctx.expr());
            if(ctx.type().getText().equalsIgnoreCase("integer")) {
                int arreglo[] = new int[(int)dimen.valor];
                nuevo.valor = arreglo;
                nuevo.identificador = ctx.IDEN().getText();
                nuevo.tipo = ctx.type().getText();
                ent.nuevoSimbolo(ctx.IDEN().getText() + TipoSimbolo.Variable.name(), nuevo);
                return true;
            }else if(ctx.type().getText().equalsIgnoreCase("real")) {
                double arreglo[] = new double[(int)dimen.valor];
                nuevo.valor = arreglo;
                nuevo.identificador = ctx.IDEN().getText();
                nuevo.tipo = ctx.type().getText();
                ent.nuevoSimbolo(ctx.IDEN().getText() + TipoSimbolo.Variable.name(), nuevo);
                return true;
            } else if(ctx.type().getText().equalsIgnoreCase("character")) {
                String arreglo[] = new String[(int)dimen.valor];
                nuevo.valor = arreglo;
                nuevo.identificador = ctx.IDEN().getText();
                nuevo.tipo = ctx.type().getText();
                ent.nuevoSimbolo(ctx.IDEN().getText() + TipoSimbolo.Variable.name(), nuevo);
                return true;
            }else{
                System.out.println("Tipo de dato para arreglo no permitido");
                errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                        "El arreglo "+ctx.IDEN().getText().toUpperCase()+ " fue declarado con un tipo de dato incompatible", ErrorCompilador.ErrorTipo.Semantico));
            }
        }else errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                    "La variable ya existe en el entorno actual.", ErrorCompilador.ErrorTipo.Semantico));
        return false;
    }

    public Object visitDeclArrayB(GramaticaParser.DeclArrayBContext ctx) {
        Entorno ent = pilaEnt.peek();
        if(!ent.TablaSimbolo.containsKey((ctx.IDEN().getText() + TipoSimbolo.Variable.name()).toUpperCase()))
        {
            Simbolo nuevo = new Simbolo("","",0,TipoSimbolo.Variable);
            Simbolo dimenUno = (Simbolo)visit(ctx.dimen1);
            Simbolo dimenDos = (Simbolo)visit(ctx.dimen2);
            if(ctx.type().getText().equalsIgnoreCase("integer")) {
                int arreglo[][] = new int[(int)dimenUno.valor][(int)dimenDos.valor];
                nuevo.valor = arreglo;
                nuevo.identificador = ctx.IDEN().getText();
                nuevo.tipo = ctx.type().getText();
                ent.nuevoSimbolo(ctx.IDEN().getText() + TipoSimbolo.Variable.name(), nuevo);
            }else if(ctx.type().getText().equalsIgnoreCase("real")) {
                double arreglo[][] = new double[(int)dimenUno.valor][(int)dimenDos.valor];
                nuevo.valor = arreglo;
                nuevo.identificador = ctx.IDEN().getText();
                nuevo.tipo = ctx.type().getText();
                ent.nuevoSimbolo(ctx.IDEN().getText() + TipoSimbolo.Variable.name(), nuevo);
            } else if(ctx.type().getText().equalsIgnoreCase("character")) {
                String arreglo[][] = new String[(int)dimenUno.valor][(int)dimenDos.valor];
                nuevo.valor = arreglo;
                nuevo.identificador = ctx.IDEN().getText();
                nuevo.tipo = ctx.type().getText();
                ent.nuevoSimbolo(ctx.IDEN().getText() + TipoSimbolo.Variable.name(), nuevo);
            }else{
                System.out.println("Tipo de dato para arreglo no permitido");
                errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                        "El arreglo "+ctx.IDEN().getText().toUpperCase()+ " fue declarado con un tipo de dato incompatible", ErrorCompilador.ErrorTipo.Semantico));
            }
        }else errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                "La variable ya existe en el entorno actual.", ErrorCompilador.ErrorTipo.Semantico));
        return false;
    }

    public Object visitLengthArray(GramaticaParser.LengthArrayContext ctx){
        Entorno ent = pilaEnt.peek();
        int tamaño = 0;
        Simbolo recived = ent.Buscar(ctx.IDEN().getText() + TipoSimbolo.Variable.name());
        if (recived.tipo.equalsIgnoreCase("integer")){
            if(recived.valor instanceof int[][]){
                int aux[][]=(int[][])recived.valor;
                tamaño = aux.length*aux[0].length;
            }else{
                int aux[]=(int[])recived.valor;
                tamaño = aux.length;
            }
        } else if (recived.tipo.equalsIgnoreCase("real")){
            if(recived.valor instanceof double[][]){
                double aux[][]=(double[][])recived.valor;
                tamaño = aux.length*aux[0].length;
            }else{
                double aux[]=(double[])recived.valor;
                tamaño = aux.length;
            }
        }else{
            if(recived.valor instanceof String[][]){
                String aux[][]=(String[][])recived.valor;
                tamaño = aux.length*aux[0].length;
            }else{
                String aux[]=(String[])recived.valor;
                tamaño = aux.length;
            }
        }
        return new Simbolo("", "INTEGER", tamaño, TipoSimbolo.Variable);
    }

    public Object visitVArrayExpr(GramaticaParser.VArrayExprContext ctx){
        Entorno ent = pilaEnt.peek();
        Simbolo recived = ent.Buscar(ctx.IDEN().getText() + TipoSimbolo.Variable.name());
        Simbolo sDimen1;
        Simbolo sDimen2;
        int dimen1 = 0;
        int dimen2 = 0;
        if (recived.tipo.equalsIgnoreCase("integer")){
            if(recived.valor instanceof int[][]){
                int aux[][]=(int[][])recived.valor;
                sDimen1 = (Simbolo)visit(ctx.dime1);
                sDimen2 = (Simbolo)visit(ctx.dime2);
                dimen1 = (int) sDimen1.valor;
                dimen2 = (int )sDimen2.valor;
                return new Simbolo("", "INTEGER", aux[dimen1-1][dimen2-1], TipoSimbolo.Variable);
            }else{
                int aux[]=(int[])recived.valor;
                sDimen1 = (Simbolo)visit(ctx.dime1);
                dimen1 = (int) sDimen1.valor;
                return new Simbolo("", "INTEGER", aux[dimen1-1], TipoSimbolo.Variable);
            }
        }else if (recived.tipo.equalsIgnoreCase("real")){
            if(recived.valor instanceof double[][]){
                double aux[][]=(double[][])recived.valor;
                sDimen1 = (Simbolo)visit(ctx.dime1);
                sDimen2 = (Simbolo)visit(ctx.dime2);
                dimen1 = (int) sDimen1.valor;
                dimen2 = (int) sDimen2.valor;
                return new Simbolo("", "REAL", aux[dimen1-1][dimen2-1], TipoSimbolo.Variable);
            }else{
                double aux[]=(double[])recived.valor;
                sDimen1 = (Simbolo)visit(ctx.dime1);
                dimen1 = (int) sDimen1.valor;
                return new Simbolo("", "REAL", aux[dimen1-1], TipoSimbolo.Variable);
            }
        }else {
            if (recived.valor instanceof String[][]) {
                String aux[][] = (String[][]) recived.valor;
                sDimen1 = (Simbolo) visit(ctx.dime1);
                sDimen2 = (Simbolo) visit(ctx.dime2);
                dimen1 = (int) sDimen1.valor;
                dimen2 = (int) sDimen2.valor;
                return new Simbolo("", "CHARACTER", aux[dimen1-1][dimen2-1], TipoSimbolo.Variable);
            } else {
                String aux[] = (String[]) recived.valor;
                sDimen1 = (Simbolo) visit(ctx.dime1);
                dimen1 = (int) sDimen1.valor;
                return new Simbolo("", "CHARACTER", aux[dimen1-1], TipoSimbolo.Variable);
            }
        }
    }  //PARA PODER IMPRIMIR LOS ARRAYS

    public Object visitAsigArray(GramaticaParser.AsigArrayContext ctx){
        Entorno ent = pilaEnt.peek();
        Simbolo sExiste = ent.Buscar(ctx.IDEN().getText() + TipoSimbolo.Variable.name());
        if(sExiste != null){
            Simbolo simboloAux = sExiste;
            if(simboloAux.tipo.equalsIgnoreCase("integer")){
                if(simboloAux.valor instanceof int[]) {
                    int arrAux[] = (int[]) simboloAux.valor;
                    int tamaño = arrAux.length;
                    if (ctx.lexpr() != null) {
                        if (ctx.lexpr().expr().size() == tamaño) {
                            for (int i = 0; i < tamaño; i++) {
                                //System.out.println(ctx.lexpr().expr().get(i).getText());
                                arrAux[i] = Integer.parseInt(ctx.lexpr().expr().get(i).getText());
                            }
                        } else {
                            System.out.println("La cantidad de elementos no coincide con el tamaño del arreglo");
                        }
                    } else {
                        Simbolo sPosi1 = (Simbolo) visit(ctx.posi);
                        Simbolo sValor = (Simbolo) visit(ctx.valor);
                        int sposi1 = (int) sPosi1.valor;
                        int svalor = (int) sValor.valor;
                        arrAux[sposi1 - 1] = svalor;
                        //int arrAux2[]=(int[])simboloAux.valor;
                        //System.out.println(String.valueOf(arrAux2[0]));
                    }
                }else{
                    int arrAux[][] = (int[][]) simboloAux.valor;
                    int tamaño = arrAux.length;
                    int tamaño2 = arrAux[0].length;
                    if (ctx.lexpr() != null) {
                        if (ctx.lexpr().expr().size() == tamaño) {
                            for (int i = 0; i < tamaño; i++) {
                                for(int j = 0; j < tamaño2; j++) {
                                    //System.out.println(ctx.lexpr().expr().get(i).getText());
                                    arrAux[i][j] = Integer.parseInt(ctx.lexpr().expr().get(i).getText());
                                }
                            }
                        } else {
                            System.out.println("La cantidad de elementos no coincide con el tamaño del arreglo");
                        }
                    } else {
                        Simbolo sPosi1 = (Simbolo) visit(ctx.posi);
                        Simbolo sPosi2 = (Simbolo) visit(ctx.posi2);
                        Simbolo sValor = (Simbolo) visit(ctx.valor);
                        int sposi1 = (int) sPosi1.valor;
                        int sposi2 = (int) sPosi2.valor;
                        int svalor = (int) sValor.valor;
                        arrAux[sposi1 - 1][sposi2-1] = svalor;
                        //int arrAux2[]=(int[])simboloAux.valor;
                        //System.out.println(String.valueOf(arrAux2[0]));
                    }
                }
            }else if(simboloAux.tipo.equalsIgnoreCase("real")){
                if(simboloAux.valor instanceof double[]) {
                    double arrAux[] = (double[]) simboloAux.valor;
                    int tamaño = arrAux.length;
                    if (ctx.lexpr() != null) {
                        if (ctx.lexpr().expr().size() == tamaño) {
                            for (int i = 0; i < tamaño; i++) {
                                System.out.println(ctx.lexpr().expr().get(i).getText());
                                arrAux[i] = Double.parseDouble(ctx.lexpr().expr().get(i).getText());
                            }
                        } else {
                            System.out.println("La cantidad de elementos no coincide con el tamaño del arreglo");
                        }
                    } else {
                        Simbolo sPosi1 = (Simbolo) visit(ctx.posi);
                        Simbolo sValor = (Simbolo) visit(ctx.valor);
                        int sposi1 = (int) sPosi1.valor;
                        double svalor = (double) sValor.valor;
                        arrAux[sposi1 - 1] = svalor;
                    }
                }else{
                    double arrAux[][] = (double[][]) simboloAux.valor;
                    int tamaño = arrAux.length;
                    int tamaño2 = arrAux[0].length;
                    if (ctx.lexpr() != null) {
                        if (ctx.lexpr().expr().size() == tamaño) {
                            for (int i = 0; i < tamaño; i++) {
                                for(int j = 0; j < tamaño2; j++) {
                                    //System.out.println(ctx.lexpr().expr().get(i).getText());
                                    arrAux[i][j] = Double.parseDouble(ctx.lexpr().expr().get(i).getText());
                                }
                            }
                        } else {
                            System.out.println("La cantidad de elementos no coincide con el tamaño del arreglo");
                        }
                    } else {
                        Simbolo sPosi1 = (Simbolo) visit(ctx.posi);
                        Simbolo sPosi2 = (Simbolo) visit(ctx.posi2);
                        Simbolo sValor = (Simbolo) visit(ctx.valor);
                        int sposi1 = (int) sPosi1.valor;
                        int sposi2 = (int) sPosi2.valor;
                        double svalor = (double) sValor.valor;
                        arrAux[sposi1 - 1][sposi2-1] = svalor;
                        //int arrAux2[]=(int[])simboloAux.valor;
                        //System.out.println(String.valueOf(arrAux2[0]));
                    }
                }
            }else{
                if(simboloAux.valor instanceof String[]) {
                    String arrAux[] = (String[]) simboloAux.valor;
                    int tamaño = arrAux.length;
                    if (ctx.lexpr() != null) {
                        if (ctx.lexpr().expr().size() == tamaño) {
                            for (int i = 0; i < tamaño; i++) {
                                System.out.println(ctx.lexpr().expr().get(i).getText());
                                arrAux[i] = ctx.lexpr().expr().get(i).getText();
                            }
                        } else {
                            System.out.println("La cantidad de elementos no coincide con el tamaño del arreglo");
                        }
                    } else {
                        Simbolo sPosi1 = (Simbolo) visit(ctx.posi);
                        Simbolo sValor = (Simbolo) visit(ctx.valor);
                        int sposi1 = (int) sPosi1.valor;
                        String svalor = sValor.valor.toString();
                        arrAux[sposi1 - 1] = svalor;
                    }
                }else{
                    String arrAux[][] = (String[][]) simboloAux.valor;
                    int tamaño = arrAux.length;
                    int tamaño2 = arrAux[0].length;
                    if (ctx.lexpr() != null) {
                        if (ctx.lexpr().expr().size() == tamaño) {
                            for (int i = 0; i < tamaño; i++) {
                                for(int j = 0; j < tamaño2; j++) {
                                    //System.out.println(ctx.lexpr().expr().get(i).getText());
                                    arrAux[i][j] = ctx.lexpr().expr().get(i).getText();
                                }
                            }
                        } else {
                            System.out.println("La cantidad de elementos no coincide con el tamaño del arreglo");
                        }
                    } else {
                        Simbolo sPosi1 = (Simbolo) visit(ctx.posi);
                        Simbolo sPosi2 = (Simbolo) visit(ctx.posi2);
                        Simbolo sValor = (Simbolo) visit(ctx.valor);
                        int sposi1 = (int) sPosi1.valor;
                        int sposi2 = (int) sPosi2.valor;
                        String svalor = (String) sValor.valor;
                        arrAux[sposi1 - 1][sposi2-1] = svalor;
                        //int arrAux2[]=(int[])simboloAux.valor;
                        //System.out.println(String.valueOf(arrAux2[0]));
                    }
                }
            }
        }else{
            errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(),ctx.IDEN().getSymbol().getCharPositionInLine(),
                    "EL arreglo "+ctx.IDEN().getText() + " no existe para ser modificada",ErrorCompilador.ErrorTipo.Semantico));
            System.out.println("EL arreglo "+ctx.IDEN().getText() +  " no existe para ser modificada");
        }
        return true;
    }

    public Object visitAsigArrayB(GramaticaParser.AsigArrayBContext ctx){
        Entorno ent = pilaEnt.peek();
        Simbolo sExiste = ent.Buscar(ctx.IDEN().getText() + TipoSimbolo.Variable.name());
        if(sExiste != null){
            Simbolo simboloAux = sExiste;
            if(simboloAux.tipo.equalsIgnoreCase("integer")) {
                int arrAux[][] = (int[][]) simboloAux.valor;
                int tamaño = arrAux.length;
                int tamaño2 = arrAux[0].length;
                Simbolo sPosi1 = (Simbolo) visit(ctx.posi1);
                Simbolo sPosi2 = (Simbolo) visit(ctx.posi2);
                Simbolo sValor = (Simbolo) visit(ctx.valor);
                int posi1 = (int)sPosi1.valor;
                int posi2 = (int)sPosi2.valor;
                int valor = (int)sValor.valor;
                for (int i = 0; i < tamaño ; i++){
                    for (int j = 0; j< tamaño2 ; j++){
                        if( i == posi1 && j == posi2){
                            arrAux[posi1-1][posi2-1]=valor;
                        }
                    }
                }
            }else if(simboloAux.tipo.equalsIgnoreCase("real")) {
                double arrAux[][] = (double[][]) simboloAux.valor;
                int tamaño = arrAux.length;
                int tamaño2 = arrAux[0].length;
                Simbolo sPosi1 = (Simbolo) visit(ctx.posi1);
                Simbolo sPosi2 = (Simbolo) visit(ctx.posi2);
                Simbolo sValor = (Simbolo) visit(ctx.valor);
                int posi1 = (int)sPosi1.valor;
                int posi2 = (int)sPosi2.valor;
                double valor = (double)sValor.valor;
                for (int i = 0; i < tamaño ; i++){
                    for (int j = 0; j< tamaño2 ; j++){
                        if( i == posi1-1 && j == posi2-1){
                            arrAux[posi1][posi2]=valor;
                        }
                    }
                }
            }else{
                String arrAux[][] = (String[][]) simboloAux.valor;
                int tamaño = arrAux.length;
                int tamaño2 = arrAux[0].length;
                Simbolo sPosi1 = (Simbolo) visit(ctx.posi1);
                Simbolo sPosi2 = (Simbolo) visit(ctx.posi2);
                Simbolo sValor = (Simbolo) visit(ctx.valor);
                int posi1 = (int)sPosi1.valor;
                int posi2 = (int)sPosi2.valor;
                String valor = (String)sValor.valor;
                for (int i = 0; i < tamaño ; i++){
                    for (int j = 0; j< tamaño2 ; j++){
                        if( i == posi1-1 && j == posi2-1){
                            arrAux[posi1][posi2]= valor;
                        }
                    }
                }

            }
        }else{
            errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(),ctx.IDEN().getSymbol().getCharPositionInLine(),
                    "EL arreglo "+ctx.IDEN().getText() + " no existe para ser modificada",ErrorCompilador.ErrorTipo.Semantico));
            System.out.println("EL arreglo "+ctx.IDEN().getText() +  " no existe para ser modificada");
        }
        return true;
    }

    public Object visitDeclDinamicA(GramaticaParser.DeclDinamicAContext ctx){
        Entorno ent = pilaEnt.peek();
        if(!ent.TablaSimbolo.containsKey((ctx.IDEN().getText() + TipoSimbolo.Variable.name()).toUpperCase())) {
            Simbolo nuevo = new Simbolo("","",0,TipoSimbolo.Variable);
            String dimen = ctx.dosp.getText();
            if(dimen.equals(":")){
                if(ctx.type().getText().equalsIgnoreCase("integer")) {
                    int arreglo[]={};
                    nuevo.valor = arreglo;
                    nuevo.identificador = ctx.IDEN().getText();
                    nuevo.tipo = ctx.type().getText();
                    ent.nuevoSimbolo(ctx.IDEN().getText() + TipoSimbolo.Variable.name(), nuevo);
                    return true;
                }else if(ctx.type().getText().equalsIgnoreCase("real")) {
                    double   arreglo[]={};
                    nuevo.valor = arreglo;
                    nuevo.identificador = ctx.IDEN().getText();
                    nuevo.tipo = ctx.type().getText();
                    ent.nuevoSimbolo(ctx.IDEN().getText() + TipoSimbolo.Variable.name(), nuevo);
                    return true;
                }else if(ctx.type().getText().equalsIgnoreCase("character")) {
                    String arreglo[]={};
                    nuevo.valor = arreglo;
                    nuevo.identificador = ctx.IDEN().getText();
                    nuevo.tipo = ctx.type().getText();
                    ent.nuevoSimbolo(ctx.IDEN().getText() + TipoSimbolo.Variable.name(), nuevo);
                    return true;
                }else{
                    System.out.println("Tipo de dato para arreglo no permitido");
                    errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                            "El arreglo "+ctx.IDEN().getText().toUpperCase()+ " fue declarado con un tipo de dato incompatible", ErrorCompilador.ErrorTipo.Semantico));
                }
            }else if(dimen.equals(":,:")){
                if(ctx.type().getText().equalsIgnoreCase("integer")) {
                    int arreglo[][]={{}};
                    nuevo.valor = arreglo;
                    nuevo.identificador = ctx.IDEN().getText();
                    nuevo.tipo = ctx.type().getText();
                    ent.nuevoSimbolo(ctx.IDEN().getText() + TipoSimbolo.Variable.name(), nuevo);
                    return true;
                }else if(ctx.type().getText().equalsIgnoreCase("real")) {
                    double arreglo[][]={{}};
                    nuevo.valor = arreglo;
                    nuevo.identificador = ctx.IDEN().getText();
                    nuevo.tipo = ctx.type().getText();
                    ent.nuevoSimbolo(ctx.IDEN().getText() + TipoSimbolo.Variable.name(), nuevo);
                    return true;
                }else if(ctx.type().getText().equalsIgnoreCase("character")) {
                    String arreglo[][]={{}};
                    nuevo.valor = arreglo;
                    nuevo.identificador = ctx.IDEN().getText();
                    nuevo.tipo = ctx.type().getText();
                    ent.nuevoSimbolo(ctx.IDEN().getText() + TipoSimbolo.Variable.name(), nuevo);
                    return true;
                }else{
                    System.out.println("Tipo de dato para arreglo no permitido");
                    errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                            "El arreglo "+ctx.IDEN().getText().toUpperCase()+ " fue declarado con un tipo de dato incompatible", ErrorCompilador.ErrorTipo.Semantico));
                }
            }
        }
        else errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                "La variable ya existe en el entorno actual.", ErrorCompilador.ErrorTipo.Semantico));
        return false;
    }

    public Object visitAsignDinamicA(GramaticaParser.AsignDinamicAContext ctx){
        Entorno ent = pilaEnt.peek();
        Simbolo recived = ent.Buscar(ctx.IDEN().getText() + TipoSimbolo.Variable.name());
        Simbolo sDimen1;
        Simbolo sDimen2;
        int dimen1 = 0;
        int dimen2 = 0;
        if(ctx.pasig.getText().equals("allocate")){
            if (recived.tipo.equalsIgnoreCase("integer")){
                if(recived.valor instanceof int[][]){
                    int aux[][] = (int[][])recived.valor;
                    if (aux.length == 0) {
                        sDimen1 = (Simbolo) visit(ctx.dimen1);
                        sDimen2 = (Simbolo) visit(ctx.dimen2);
                        dimen1 = (int) sDimen1.valor;
                        dimen2 = (int) sDimen2.valor;
                        int nuevoa[][] = new int[dimen1][dimen2];
                        recived.valor = nuevoa;
                        return true;
                    }else{
                        System.out.println("No es posible usar ALLOCATE en arreglo "+recived.identificador +" CON dimensiones");
                        errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                                "No es posible usar ALLOCATE en arreglo "+recived.identificador +" CON dimensiones", ErrorCompilador.ErrorTipo.Semantico));
                        return false;
                    }
                }else{
                    int aux[] = (int[])recived.valor;
                    if (aux.length == 0) {
                        sDimen1 = (Simbolo) visit(ctx.dimen1);
                        dimen1 = (int) sDimen1.valor;
                        int nuevoa[] = new int[dimen1];
                        recived.valor = nuevoa;
                        return true;
                    }else{
                        System.out.println("No es posible usar ALLOCATE en arreglo "+recived.identificador +" CON dimensiones");
                        errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                                "No es posible usar ALLOCATE en arreglo "+recived.identificador +" CON dimensiones", ErrorCompilador.ErrorTipo.Semantico));
                        return false;
                    }
                }
            } else if (recived.tipo.equalsIgnoreCase("real")) {
                if (recived.valor instanceof double[][]) {
                    double aux[][] = (double[][])recived.valor;
                    if (aux.length == 0) {
                        sDimen1 = (Simbolo) visit(ctx.dimen1);
                        sDimen2 = (Simbolo) visit(ctx.dimen2);
                        dimen1 = (int) sDimen1.valor;
                        dimen2 = (int) sDimen2.valor;
                        double nuevoa[][] = new double[dimen1][dimen2];
                        recived.valor = nuevoa;
                        return true;
                    }else{
                        System.out.println("No es posible usar ALLOCATE en arreglo "+recived.identificador +" CON dimensiones");
                        errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                                "No es posible usar ALLOCATE en arreglo "+recived.identificador +" CON dimensiones", ErrorCompilador.ErrorTipo.Semantico));
                        return false;
                    }
                } else {
                    double aux[] = (double[])recived.valor;
                    if (aux.length == 0) {
                        sDimen1 = (Simbolo) visit(ctx.dimen1);
                        dimen1 = (int) sDimen1.valor;
                        double nuevoa[] = new double[dimen1];
                        recived.valor = nuevoa;
                        return true;
                    }else{
                        System.out.println("No es posible usar ALLOCATE en arreglo "+recived.identificador +" CON dimensiones");
                        errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                                    "No es posible usar ALLOCATE en arreglo "+recived.identificador +" CON dimensiones", ErrorCompilador.ErrorTipo.Semantico));
                        return false;
                    }

                }
            }else{
                if (recived.valor instanceof String[][]) {
                    String aux[][] = (String[][])recived.valor;
                    if (aux.length == 0) {
                        sDimen1 = (Simbolo) visit(ctx.dimen1);
                        sDimen2 = (Simbolo) visit(ctx.dimen2);
                        dimen1 = (int) sDimen1.valor;
                        dimen2 = (int) sDimen2.valor;
                        String nuevoa[][] = new String[dimen1][dimen2];
                        recived.valor = nuevoa;
                        return true;
                    }else{
                        System.out.println("No es posible usar ALLOCATE en arreglo "+recived.identificador +" CON dimensiones");
                        errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                                "No es posible usar ALLOCATE en arreglo "+recived.identificador +" CON dimensiones", ErrorCompilador.ErrorTipo.Semantico));
                        return false;
                    }
                } else {
                    String aux[] = (String[])recived.valor;
                    if (aux.length == 0) {
                        sDimen1 = (Simbolo) visit(ctx.dimen1);
                        dimen1 = (int) sDimen1.valor;
                        String nuevoa[] = new String[dimen1];
                        recived.valor = nuevoa;
                        return true;
                    }else{
                        System.out.println("No es posible usar ALLOCATE en arreglo "+recived.identificador +" CON dimensiones");
                        errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                                "No es posible usar ALLOCATE en arreglo "+recived.identificador +" CON dimensiones", ErrorCompilador.ErrorTipo.Semantico));
                        return false;
                    }
                }
            }
        }else if(ctx.pasig.getText().equals("deallocate")){

            if (recived.tipo.equalsIgnoreCase("integer")) {

                if (recived.valor instanceof int[][]) {
                    int aux[][] = (int[][])recived.valor;
                    if (aux.length != 0) {
                        int nuevoa[][] = {{}};
                        recived.valor = nuevoa;
                        return true;
                    }else{
                        System.out.println("No es posible usar DEALLOCATE en arreglo "+recived.identificador +" SIN dimensiones");
                        errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                                "No es posible usar DEALLOCATE en arreglo "+recived.identificador +" SIN dimensiones", ErrorCompilador.ErrorTipo.Semantico));
                        return false;
                    }

                }else{
                    int aux[] = (int[])recived.valor;
                    if (aux.length != 0) {
                        int nuevoa[] = {};
                        recived.valor = nuevoa;
                        return true;
                    }else{
                        System.out.println("No es posible usar DEALLOCATE en arreglo "+recived.identificador +" SIN dimensiones");
                        errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                                "No es posible usar DEALLOCATE en arreglo "+recived.identificador +" SIN dimensiones", ErrorCompilador.ErrorTipo.Semantico));
                        return false;
                    }
                }


            }else if (recived.tipo.equalsIgnoreCase("real")) {
                if (recived.valor instanceof double[][]) {
                    double aux[][] = (double[][])recived.valor;
                    if (aux.length != 0) {
                        double nuevoa[][] = {{}};
                        recived.valor = nuevoa;
                        return true;
                    }else{
                        System.out.println("No es posible usar DEALLOCATE en arreglo "+recived.identificador +" SIN dimensiones");
                        errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                            "No es posible usar DEALLOCATE en arreglo "+recived.identificador +" SIN dimensiones", ErrorCompilador.ErrorTipo.Semantico));
                        return false;
                    }
                }else{
                    double aux[] = (double[])recived.valor;
                    if (aux.length != 0) {
                        double nuevoa[] = {};
                        recived.valor = nuevoa;
                        return true;
                    }else{
                        System.out.println("No es posible usar DEALLOCATE en arreglo "+recived.identificador +" SIN dimensiones");
                        errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                            "No es posible usar DEALLOCATE en arreglo "+recived.identificador +" SIN dimensiones", ErrorCompilador.ErrorTipo.Semantico));
                        return false;
                    }
                }


            }else{
                if (recived.valor instanceof String[][]) {
                    String aux[][] = (String[][])recived.valor;
                    if (aux.length != 0) {
                        String nuevoa[][] = {{}};
                        recived.valor = nuevoa;
                        return true;
                    }else{
                        System.out.println("No es posible usar DEALLOCATE en arreglo "+recived.identificador +" SIN dimensiones");
                        errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                                "No es posible usar DEALLOCATE en arreglo "+recived.identificador +" SIN dimensiones", ErrorCompilador.ErrorTipo.Semantico));
                        return false;
                    }
                }else{
                    String aux[] = (String[])recived.valor;
                    if (aux.length != 0) {
                        String nuevoa[] = {};
                        recived.valor = nuevoa;
                        return true;
                    }else{
                        System.out.println("No es posible usar DEALLOCATE en arreglo "+recived.identificador +" SIN dimensiones");
                        errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                                "No es posible usar DEALLOCATE en arreglo "+recived.identificador +" SIN dimensiones", ErrorCompilador.ErrorTipo.Semantico));
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public String visitType(GramaticaParser.TypeContext ctx)
    {
        return ctx.getText();
    }

    public Object visitOpExpr(GramaticaParser.OpExprContext ctx){
        //System.out.println("ENTRE A LAS OPERACIONES");
        Simbolo izq = (Simbolo)visit(ctx.left);
        Simbolo der = (Simbolo)visit(ctx.right);
        String operacion = ctx.op.getText();

        if (operacion.equals("**")) {
            if(izq.tipo.equalsIgnoreCase("INTEGER") && der.tipo.equalsIgnoreCase("INTEGER")){
                return new Simbolo("", "INTEGER", Math.pow((int) izq.valor,(int) der.valor), TipoSimbolo.Variable);
            }else if (izq.tipo.equalsIgnoreCase("INTEGER") && der.tipo.equalsIgnoreCase("REAL")) {
                return new Simbolo("", "REAL", Math.pow((int) izq.valor,(double) der.valor), TipoSimbolo.Variable);
            } else if (izq.tipo.equalsIgnoreCase("REAL") && der.tipo.equalsIgnoreCase("INTEGER")) {
                return new Simbolo("", "REAL", Math.pow((double) izq.valor,(int) der.valor), TipoSimbolo.Variable);
            }else if (izq.tipo.equalsIgnoreCase("REAL") && der.tipo.equalsIgnoreCase("REAL")) {
                return new Simbolo("", "REAL", Math.pow((double) izq.valor,(double) der.valor), TipoSimbolo.Variable);
            }
            else{
                throw new IllegalArgumentException("Elevacion no válida, revisa tus datos");
            }
        }else {
            if (izq.tipo.equalsIgnoreCase("INTEGER") && der.tipo.equalsIgnoreCase("INTEGER")) {
                return switch (operacion.charAt(0)) {
                    case '*' -> new Simbolo("", "INTEGER", (int) izq.valor * (int) der.valor, TipoSimbolo.Variable);
                    case '/' -> new Simbolo("", "INTEGER", (int) izq.valor / (int) der.valor, TipoSimbolo.Variable);
                    case '+' -> new Simbolo("", "INTEGER", (int) izq.valor + (int) der.valor, TipoSimbolo.Variable);
                    case '-' -> new Simbolo("", "INTEGER", (int) izq.valor - (int) der.valor, TipoSimbolo.Variable);
                    default -> throw new IllegalArgumentException("Operación no válida");
                };
            } else if (izq.tipo.equalsIgnoreCase("INTEGER") && der.tipo.equalsIgnoreCase("REAL")) {
                return switch (operacion.charAt(0)) {
                    case '*' -> new Simbolo("", "REAL", (int) izq.valor * (double) der.valor, TipoSimbolo.Variable);
                    case '/' -> new Simbolo("", "REAL", (int) izq.valor / (double) der.valor, TipoSimbolo.Variable);
                    case '+' -> new Simbolo("", "REAL", (int) izq.valor + (double) der.valor, TipoSimbolo.Variable);
                    case '-' -> new Simbolo("", "REAL", (int) izq.valor - (double) der.valor, TipoSimbolo.Variable);
                    default -> throw new IllegalArgumentException("Operación no válida");
                };
            } else if (izq.tipo.equalsIgnoreCase("REAL") && der.tipo.equalsIgnoreCase("INTEGER")) {
                return switch (operacion.charAt(0)) {
                    case '*' -> new Simbolo("", "REAL", (double) izq.valor * (int) der.valor, TipoSimbolo.Variable);
                    case '/' -> new Simbolo("", "REAL", (double) izq.valor / (int) der.valor, TipoSimbolo.Variable);
                    case '+' -> new Simbolo("", "REAL", (double) izq.valor + (int) der.valor, TipoSimbolo.Variable);
                    case '-' -> new Simbolo("", "REAL", (double) izq.valor - (int) der.valor, TipoSimbolo.Variable);
                    default -> throw new IllegalArgumentException("Operación no válida");
                };
            } else if (izq.tipo.equalsIgnoreCase("REAL") && der.tipo.equalsIgnoreCase("REAL")) {
                return switch (operacion.charAt(0)) {
                    case '*' -> new Simbolo("", "REAL", (double) izq.valor * (double) der.valor, TipoSimbolo.Variable);
                    case '/' -> new Simbolo("", "REAL", (double) izq.valor / (double) der.valor, TipoSimbolo.Variable);
                    case '+' -> new Simbolo("", "REAL", (double) izq.valor + (double) der.valor, TipoSimbolo.Variable);
                    case '-' -> new Simbolo("", "REAL", (double) izq.valor - (double) der.valor, TipoSimbolo.Variable);
                    default -> throw new IllegalArgumentException("Operación no válida");
                };
            } else {
                throw new IllegalArgumentException("Operación no válida");
            }
        }
    }

    public Object visitOpExprRacional(GramaticaParser.OpExprRacionalContext ctx){
        Simbolo izq = (Simbolo)visit(ctx.left);
        Simbolo der = (Simbolo)visit(ctx.right);
        if(izq.valor.equals(".true.")){
            izq.valor = "T";
        }else if(izq.valor.equals(".false.")){
            izq.valor = "F";
        }

        if(der.valor.equals(".true.")){
            der.valor = "T";
        }else if(der.valor.equals(".false.")){
            der.valor = "F";
        }

        String operacion = ctx.opr.getText();

        //System.out.println(izq.tipo);
        //System.out.println(der.tipo);
        //System.out.println(operacion);

        if(operacion.equals("==") || operacion.equals("/=") || operacion.equals(".eq.") || operacion.equals(".ne.") ){
            if(izq.tipo.equalsIgnoreCase("INTEGER") && der.tipo.equalsIgnoreCase("INTEGER")){
                return switch (operacion) {
                    case "==", ".eq." -> new Simbolo("", "LOGICAL", ((int) izq.valor == (int)der.valor?"T":"F"), TipoSimbolo.Variable);
                    case "/=", ".ne." -> new Simbolo("", "LOGICAL", ((int) izq.valor != (int)der.valor?"T":"F"), TipoSimbolo.Variable);
                    default -> throw new IllegalArgumentException("Operación no válida");
                };
            } else if(izq.tipo.equalsIgnoreCase("REAL") && der.tipo.equalsIgnoreCase("REAL")){
                return switch (operacion) {
                    case "==", ".eq." -> new Simbolo("", "LOGICAL", ((double) izq.valor == (double)der.valor?"T":"F"), TipoSimbolo.Variable);
                    case "/=", ".ne." -> new Simbolo("", "LOGICAL", ((double) izq.valor != (double)der.valor?"T":"F"), TipoSimbolo.Variable);
                    default -> throw new IllegalArgumentException("Operación no válida");
                };
            }else if(izq.tipo.equalsIgnoreCase("LOGICAL") && der.tipo.equalsIgnoreCase("LOGICAL")){
                return switch (operacion) {
                    case "==", ".eq." -> new Simbolo("", "LOGICAL", ( izq.valor == der.valor?"T":"F"), TipoSimbolo.Variable);
                    case "/=", ".ne." -> new Simbolo("", "LOGICAL", ( izq.valor != der.valor?"T":"F"), TipoSimbolo.Variable);
                    default -> throw new IllegalArgumentException("Operación no válida");
                };
            }else if(izq.tipo.equalsIgnoreCase("CHARACTER") && der.tipo.equalsIgnoreCase("CHARACTER")){
                return switch (operacion) {
                    case "==", ".eq." -> new Simbolo("", "LOGICAL", ( izq.valor == der.valor?"T":"F"), TipoSimbolo.Variable);
                    case "/=", ".ne." -> new Simbolo("", "LOGICAL", ( izq.valor != der.valor?"T":"F"), TipoSimbolo.Variable);
                    default -> throw new IllegalArgumentException("Operación no válida");
                };
            }else{
                System.out.println("Operación relacional no válida, revisa tu tipos de dato");
                throw new IllegalArgumentException("Operación relacional no válida, revisa tu tipos de dato");
            }
        }else if(operacion.equals(">") || operacion.equals("<") || operacion.equals(">=") || operacion.equals("<=")
        ||operacion.equals(".gt.") || operacion.equals(".lt.") || operacion.equals(".ge.") || operacion.equals(".le.") ){
            if(izq.tipo.equalsIgnoreCase("INTEGER") && der.tipo.equalsIgnoreCase("INTEGER")){
                return switch (operacion) {
                    case ">", ".gt." -> new Simbolo("", "LOGICAL", ((int) izq.valor > (int)der.valor?"T":"F"), TipoSimbolo.Variable);
                    case "<", ".lt." -> new Simbolo("", "LOGICAL", ((int) izq.valor < (int)der.valor?"T":"F"), TipoSimbolo.Variable);
                    case ">=", ".ge." -> new Simbolo("", "LOGICAL", ((int) izq.valor >= (int)der.valor?"T":"F"), TipoSimbolo.Variable);
                    case "<=", ".le." -> new Simbolo("", "LOGICAL", ((int) izq.valor <= (int)der.valor?"T":"F"), TipoSimbolo.Variable);
                    default -> throw new IllegalArgumentException("Operación no válida");
                };
            }else if(izq.tipo.equalsIgnoreCase("REAL") && der.tipo.equalsIgnoreCase("REAL")){
                return switch (operacion) {
                    case ">", ".gt." -> new Simbolo("", "LOGICAL", ((double) izq.valor > (double)der.valor?"T" : "F"), TipoSimbolo.Variable);
                    case "<", ".lt." -> new Simbolo("", "LOGICAL", ((double) izq.valor < (double)der.valor?"T" : "F"), TipoSimbolo.Variable);
                    case ">=", ".ge." -> new Simbolo("", "LOGICAL", ((double) izq.valor >= (double)der.valor?"T" : "F"), TipoSimbolo.Variable);
                    case "<=", ".le." -> new Simbolo("", "LOGICAL", ((double) izq.valor <= (double)der.valor?"T" : "F"), TipoSimbolo.Variable);
                    default -> throw new IllegalArgumentException("Operación no válida");
                };
            }else {
                System.out.println("Operación relacional no válida, revisa tu tipos de dato");
                throw new IllegalArgumentException("Operación relacional no válida, revisa tu tipos de dato");
            }
        }

    return true;
    }

    public Object visitOpExprLogica(GramaticaParser.OpExprLogicaContext ctx){
        Simbolo izq = (Simbolo)visit(ctx.left);
        Simbolo der = (Simbolo)visit(ctx.right);
        String operacion = ctx.opl.getText();
        return switch (operacion) {
            case ".and." -> new Simbolo("", "LOGICAL", (izq.valor == "T" && der.valor == "T" ? "T" : "F"), TipoSimbolo.Variable);
            case ".or." -> new Simbolo("", "LOGICAL", (izq.valor == "T" || der.valor == "T" ? "T" : "F"), TipoSimbolo.Variable);
            default -> throw new IllegalArgumentException("Operación logica no válida");
        };
    }

    public Object visitNotExpr(GramaticaParser.NotExprContext ctx){
        Simbolo expr = (Simbolo) visit(ctx.left);
        if(expr.valor.equals(".true.")){
            expr.valor = "T";
        }else if(expr.valor.equals(".false.")){
            expr.valor = "F";
        }
        boolean valor = expr.valor.equals("T");
        String va = valor ? "F" : "T";
        return new Simbolo("","LOGICAL",va,TipoSimbolo.Variable);
    }

    public Object visitIfStructure(GramaticaParser.IfStructureContext ctx) {
        //System.out.println("ENTRE AL IF");
        Simbolo svisitado;
        if ((ctx.lELSEIF() == null || ctx.lELSEIF().isEmpty()) && ctx.ELSER() == null) { //NO HAY ELSEIF NI ELSE, ENTONCES SE EVALUA SOLO EL IF
            svisitado = (Simbolo)(visit(ctx.expr()));
            if (svisitado.valor.equals("T")) {
                pilaEnt.push(new Entorno(pilaEnt.peek()));
                Object vL=visitLinstrucciones((GramaticaParser.LinstruccionesContext) ctx.list1);
                pilaEnt.pop();
                return vL;
            }
        } else if ((ctx.lELSEIF() == null || ctx.lELSEIF().isEmpty()) && ctx.ELSER() != null) {  //NO HAY ELSEIF PERO SI ELSE, ENTONCES SE EVALUA IF Y ELSE
            svisitado = (Simbolo)(visit(ctx.expr()));
            if (svisitado.valor.equals("T")) {
                pilaEnt.push(new Entorno(pilaEnt.peek()));
                Object vL=visitLinstrucciones((GramaticaParser.LinstruccionesContext) ctx.list1);
                pilaEnt.pop();
                return vL;
            } else {
                pilaEnt.push(new Entorno(pilaEnt.peek()));
                Object vL=visitLinstrucciones((GramaticaParser.LinstruccionesContext) ctx.list2);
                pilaEnt.pop();
                return vL;
            }
        } else if (ctx.lELSEIF() != null && ctx.ELSER() == null) {  //HAY ELSEIF PERO NO ELSE, ENTONCES SE EVALUA IF Y ELSEIF
            svisitado = (Simbolo)(visit(ctx.expr()));
            if (svisitado.valor.equals("T")) {
                pilaEnt.push(new Entorno(pilaEnt.peek()));
                Object vL=visitLinstrucciones((GramaticaParser.LinstruccionesContext) ctx.list1);
                pilaEnt.pop();
                return vL;
            } else {
                for (GramaticaParser.LELSEIFContext ectx : ctx.lELSEIF()) {
                    visitLELSEIF(ectx);
                }
            }
        }else if (ctx.lELSEIF() != null && ctx.ELSER() != null) {   //HAY ELSEIF Y ELSE, ENTONCES SE EVALUA IF, ELSEIF Y ELSE
            svisitado = (Simbolo)(visit(ctx.expr()));
            if (svisitado.valor.equals("T")) {
                pilaEnt.push(new Entorno(pilaEnt.peek()));
                Object vL=visitLinstrucciones((GramaticaParser.LinstruccionesContext) ctx.list1);
                pilaEnt.pop();
                return vL;
            }else{
                boolean sielse= false;
                for (GramaticaParser.LELSEIFContext ectx : ctx.lELSEIF()) {
                    sielse = (boolean)visitLELSEIF(ectx);
                }
                if(sielse){
                    pilaEnt.push(new Entorno(pilaEnt.peek()));
                    Object vL=visitLinstrucciones((GramaticaParser.LinstruccionesContext) ctx.list2);
                    pilaEnt.pop();
                    return vL;
                }
            }
        }
        return true;
    }

    public Object visitLELSEIF(GramaticaParser.LELSEIFContext ctx){
        Simbolo svisitado;
        for(int i=0; i<ctx.elseif().size(); i++){   //SE RECORREN LAS INSTRUCIONES DEL ELSEIF
            svisitado = (Simbolo)(visit(ctx.elseif().get(i).expr()));
            if(svisitado.valor.equals("T")){
                pilaEnt.push(new Entorno(pilaEnt.peek()));
                visitLinstrucciones((GramaticaParser.LinstruccionesContext) ctx.elseif().get(i).linstrucciones());
                pilaEnt.pop();
                return false;
            }
        }
        return true;
    }

    public Object visitDoStructure(GramaticaParser.DoStructureContext ctx) {
        System.out.println("ENTRE DO");
        Entorno ent = pilaEnt.peek();
        Simbolo variableInicial = ent.Buscar(ctx.IDEN().getText()+TipoSimbolo.Variable.name());
        Simbolo vInicio = (Simbolo)visit(ctx.vinicio);
        Simbolo vFinal = (Simbolo)visit(ctx.vfinal);
        Simbolo vPaso;
        int vpaso;
        if(ctx.paso == null){
            vpaso = 1;
        }else{
            vPaso=(Simbolo)visit(ctx.paso);
            vpaso = (int)vPaso.valor;
        }
        int vfinal = (int)vFinal.valor;
        if(variableInicial != null){
            variableInicial.valor=vInicio.valor;
            for(int vinicio=(int)vInicio.valor; vinicio <= vfinal; vinicio+=vpaso){
                pilaEnt.push(new Entorno(pilaEnt.peek()));
                Object vL=visitLinstrucciones((GramaticaParser.LinstruccionesContext) ctx.linstrucciones());
                pilaEnt.pop();
                variableInicial.valor=(int) variableInicial.valor + vpaso;
                if(vL instanceof Integer ){
                    continue;
                }else if(!((Boolean) vL)){
                    return false;
                }
            }
        }else{
            errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(),ctx.IDEN().getSymbol().getCharPositionInLine(),
                    "La variable "+ ctx.IDEN().getText() +" no esta declarada",ErrorCompilador.ErrorTipo.Semantico));
            System.out.println("La variable no esta declarada");
            return false;
        }


        return true;
    }

    public Object visitDoWhileStructure(GramaticaParser.DoWhileStructureContext ctx) {
        Entorno ent = pilaEnt.peek();
        Simbolo condicion = (Simbolo)visit(ctx.expr());
        while(condicion.valor.equals("T")){
            pilaEnt.push(new Entorno(pilaEnt.peek()));
            Object vL=visitLinstrucciones((GramaticaParser.LinstruccionesContext) ctx.linstrucciones());
            pilaEnt.pop();
            condicion = (Simbolo)visit(ctx.expr());
            if(!((Boolean) vL)){
                return false;
            }
        }
        return true;
    }

    public Simbolo visitAtomExpr(GramaticaParser.AtomExprContext ctx)
    {
        //System.out.println("----atom--");
        //System.out.println(ctx.getText());
        if(ctx.getText().isEmpty()){
            return new Simbolo("", "INTEGER", 0, TipoSimbolo.Variable);
        }else{
            return new Simbolo("", "INTEGER", Integer.valueOf(ctx.getText()), TipoSimbolo.Variable);
        }

    }

    public Simbolo visitParenExpr(GramaticaParser.ParenExprContext ctx){
        return (Simbolo) visit(ctx.expr());
    }
    public Simbolo visitVrealExpr(GramaticaParser.VrealExprContext ctx)
    {
        if(ctx.getText().isEmpty()){
            return new Simbolo("", "REAL", 0.00000000, TipoSimbolo.Variable);
        }else{
            return new Simbolo("", "REAL", Double.valueOf(ctx.getText()), TipoSimbolo.Variable);
        }
    }

    public Simbolo visitVcomplexExpr(GramaticaParser.VcomplexExprContext ctx)
    {
        return new Simbolo("", "COMPLEX", "9.192517926E-43,0.00000000", TipoSimbolo.Variable);
    }

    public Simbolo visitVcharacterExpr(GramaticaParser.VcharacterExprContext ctx)
    {
        if(ctx.getText().isEmpty()){
            return new Simbolo("", "CHARACTER", "", TipoSimbolo.Variable);
        }else{
            String cosa = ctx.getText();
            return new Simbolo("", "CHARACTER", ctx.getText().charAt(1), TipoSimbolo.Variable);
        }
    }

    public Simbolo visitVbooleanExpr(GramaticaParser.VbooleanExprContext ctx)
    {
        if(ctx.getText().isEmpty()){
            return new Simbolo("", "LOGICAL", "F", TipoSimbolo.Variable);
        }else{
            String cosa = ctx.getText();
            return new Simbolo("", "LOGICAL", ctx.getText(), TipoSimbolo.Variable);
        }
    }

    public Simbolo visitStrExpr(GramaticaParser.StrExprContext ctx)
    {
        char[] palabra = ctx.getText().toCharArray();
        String nuevo="";
        for(int i=1; i<palabra.length-1;i++){
            nuevo=nuevo+palabra[i];
        }
        return new Simbolo("", "STRING", nuevo, TipoSimbolo.Variable);
    }

    public Simbolo visitIdExpr(GramaticaParser.IdExprContext ctx)
    {
        Entorno ent = pilaEnt.peek();
        Simbolo id = ent.Buscar(ctx.IDEN().getText() + TipoSimbolo.Variable.name());
        if (id != null) return id;

        errores.add(new ErrorCompilador(ctx.IDEN().getSymbol().getLine(), ctx.IDEN().getSymbol().getCharPositionInLine(),
                "La variable " + ctx.IDEN().getText() + " no existe.", ErrorCompilador.ErrorTipo.Semantico));
        return null;
    }


}
