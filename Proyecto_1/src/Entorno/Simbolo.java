package Entorno;

public class Simbolo {
    public String tipo;
    public Object valor;
    public String identificador;

    public TipoSimbolo tipoSimbolo;

    public String simbNombre;
    public enum TipoSimbolo
    {
        Variable,
        Funcion,
        Subrutina,
        Parametros,
        Program,
        Nativo,
        Arreglo,
        C3D
    }

    public Simbolo(String id, String tipo, Object valor, TipoSimbolo tipoS) {
        this.identificador = id;
        this.tipo = tipo;
        this.valor = valor;
        this.tipoSimbolo = tipoS;
    }

    public Simbolo( TipoSimbolo tipoS, Object valor, String tipo ){
        this.identificador = "";
        this.tipoSimbolo = tipoS;
        this.valor = valor;
        this.tipo = tipo;
    }
}
